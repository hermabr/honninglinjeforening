import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";

function NotFound() {
  const router = useRouter();

  return (
    <div className="grid h-screen mx-5 place-items-center">
      <span className="text-3xl font-bold text-white">
        <span>
          Din luring! Siden{" "}
          <span className="text-pink-300">{router.asPath}</span> finnes
          dessverre ikke (enda), men kanskje du har lyst til å trylle frem
          frontend-kunnskapene og{" "}
        </span>
        <Link passHref href="https://gitlab.com/hermabr/honninglinjeforening">
          <a className="underline" target="_blank" rel="noopener noreferrer">
            lage den til selv
          </a>
        </Link>
        <span>?</span>
      </span>
    </div>
  );
}

export default NotFound;
