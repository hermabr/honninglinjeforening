import sgMail from '@sendgrid/mail'
import type { NextApiRequest, NextApiResponse } from 'next'

sgMail.setApiKey(process.env.SENDGRID_API_KEY!)

type RequestData = {
  message: string,
  email: string
}

type ResponseData = {
  message: string,
  success: boolean
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResponseData>
) {
  const email = req.body.email;
  const message = req.body.message;
  console.log(req.body)
  console.log(email)
  console.log(message)
  const msg = {
    to: 'simon.halstensen@gmail.com', // Change to your recipient
    from: 'lolthekiller321@gmail.com', // Change to your verified sender
    subject: 'Honning Linjeforening kontaktform',
    text: `E-post: ${email}\n\nMelding: ${message}`,
  }
  console.log(msg)

  sgMail
    .send(msg)
    .then(() => {
      res.status(200).json({ message: `Takk for interessen! Vi tar kontakt på "${email}" så fort som mulig`, success: true })
    })
    .catch((error) => {
      res.status(500).json({ message: 'Error when sending the email', success: false })
    })
}
