import React from "react";
import ContactComponent from "../components/ContactComponent";
import SEO from "../components/SEO";

type PropsTypes = {
  title: string;
};

function Contact({ title }: PropsTypes) {
  return (
    <>
      <SEO title="Kontakt oss" />
      <div className="pt-24 lg:pt-32">
        <ContactComponent title="Kontakt oss" />
      </div>
    </>
  );
}

export default Contact;
