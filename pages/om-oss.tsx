import Head from "next/head";
import Link from "next/link";
import React from "react";
import SEO from "../components/SEO";

function AboutUs() {
  return (
    <>
      <SEO title="Om oss" />

      <Link href="https://www.uio.no/studier/program/honours-programmet/">
        <a>
          <div className="grid h-screen place-items-center">
            <div className="mx-4">
              <h4 className="text-xl text-gold">Om oss:</h4>
              <h1 className="text-5xl font-bold text-gold">
                Honours-programmet.
              </h1>
            </div>
          </div>
        </a>
      </Link>
    </>
  );
}

export default AboutUs;
