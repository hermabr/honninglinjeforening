import type { NextPage } from "next";
import Hero from "../components/Hero";
import SEO from "../components/SEO";

import React from "react";

function Home() {
  return (
    <>
      <SEO title="Hjem" />
      <Hero />
    </>
  );
}

export default Home;
