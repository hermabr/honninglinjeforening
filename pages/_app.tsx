import "../styles/globals.css";
import type { AppProps } from "next/app";
import Navbar from "../components/Navbar";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Navbar />
      <Component {...pageProps} />
      <style jsx global>{`
        body {
          background: rgb(23 23 23);
        }
      `}</style>
    </>
  );
}

export default MyApp;
