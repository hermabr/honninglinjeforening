import Head from "next/head";
import Image from "next/image";
import React from "react";

function OskarOst() {
  return (
    <>
      <Head>
        <title>Oskar Ost | Honning - honours-programmet</title>
        <meta name="robots" content="noindex" />
      </Head>
      <div className="grid w-screen h-screen grid-cols-1 md:grid-cols-2">
        <div className="relative">
          <Image
            src={"/images/oskar-ost.jpeg"}
            layout={"fill"}
            objectFit={"contain"}
            alt="Honours-elev Oskar Ost i porno-kjelleren"
          />
        </div>
        <div className="relative">
          <Image
            src={"/images/pornokjeller-line-jonatan.jpeg"}
            layout={"fill"}
            objectFit={"contain"}
            alt="Honours-elev Oskar Ost i porno-kjelleren"
          />
        </div>
      </div>
    </>
  );
}

export default OskarOst;
