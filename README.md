# Honnings linjeforening

## Getting Started

### Frontend framework

[next.js](https://nextjs.org/)

### Css framework

[tailwind.css](https://tailwindcss.com/)

### Hosting

[vercel.app](https://vercel.com/), login: lfhonning.uio \[curly-a\] gmail.com

### Domain provider

[domeneshop](https://domene.shop/), login: lfhonning.uio \[curly-a\] gmail.com
