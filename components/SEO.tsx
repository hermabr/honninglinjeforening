import Head from "next/head";

import React from "react";

function SEO({ title }: { title: string }) {
  return (
    <Head>
      <title>{title} | Honning - honours-programmet</title>
      <meta
        name="description"
        content="Honours-programmet er et tverrfaglig studium ved Universitetet i Oslo
          med studenter innenfor humaniora, samfunnsvitenskap og realfag."
      />
    </Head>
  );
}

export default SEO;
