import React, { useState } from "react";
// import { SepColorEnum } from "./ColorSeperator";
// import SectionTitle from "./SectionTitle";
// import no from "locales/no";

type PropsTypes = {
  title: string;
};

function ContactComponent({ title }: PropsTypes) {
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");

  const reset = () => {
    setEmail("");
    setMessage("");
  };

  const isValidEmail = (email: string) => {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
  };

  const sendMail = async (e: { preventDefault: () => void }) => {
    e.preventDefault();

    if (email == "" && message == "") {
      alert(
        "Du mangler e-post og melding. Ta kontakt på simon.halstensen@gmail.com hvis problemet vedvarer"
      );
    } else if (email == "") {
      alert(
        "Du mangler e-post. Ta kontakt på simon.halstensen@gmail.com hvis problemet vedvarer"
      );
    } else if (message == "") {
      alert(
        "Du mangler melding. Ta kontakt på simon.halstensen@gmail.com hvis problemet vedvarer"
      );
    } else {
      if (isValidEmail(email)) {
        try {
          await fetch("/api/contact", {
            method: "POST",
            headers: { "content-type": "application/json" },
            body: JSON.stringify({ email, message }),
          })
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              if (data.success) {
                reset();
                alert(data.message);
              } else {
                alert(
                  "Noe gikk galt i sendingen av meldingen. Ta kontakt på simon.halstensen@gmail.com hvis problemet vedvarer"
                );
              }
            });
        } catch (error) {
          alert(
            "Det oppsto feil i kontaktskjemaet. Ta kontakt på simon.halstensen@gmail.com"
          );
        }
      } else {
        alert(
          "Du ser ut til å ha brukte en ugyldig epost. Hvis problemet vedvarer: Ta kontakt på simon.halstensen@gmail.com"
        );
      }
    }
  };

  return (
    <section id="contact">
      <div className="my-10 text-center">
        <h2 className="text-5xl font-bold text-white mt-7">
          Lurer du på noe? Kontakt oss
        </h2>
        <span className="inline-block w-[30px] h-[10px] mt-5 bg-gold"></span>
      </div>
      <form
        className="grid gap-3 place-items-center px-7 sm:px-0"
        onSubmit={sendMail}
        method="POST"
      >
        <div className="flex flex-col w-full space-y-1 sm:w-2/3">
          <label htmlFor="email" className="text-neutral-100">
            E-post
          </label>
          <input
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
            type="email"
            id="email"
            name="email"
            className="px-4 py-3 rounded appearance-none bg-neutral-100 border-neutral-100 hover:cursor-text focus:border-gray-500 border-1 focus:bg-white focus:outline-none"
            placeholder="jakob@sin.mail"
          />
        </div>
        <div className="flex flex-col w-full space-y-1 sm:w-2/3">
          <label htmlFor="message" className="text-neutral-100">
            Melding
          </label>
          <textarea
            value={message}
            onChange={(e) => {
              setMessage(e.target.value);
            }}
            name="message"
            id="message"
            className="h-48 px-4 py-3 rounded appearance-none resize-none bg-neutral-100 border-neutral-100 hover:cursor-text focus:border-gray-500 border-1 focus:bg-white focus:outline-none "
            placeholder="Hva lurer du på.."
          ></textarea>
        </div>
        <div className="w-full text-center sm:w-2/3">
          <button
            className="px-4 py-2 font-bold text-black rounded shadow bg-gold focus:shadow-outline focus:outline-none hover:cursor-pointer"
            type="submit"
          >
            Send
          </button>
        </div>
      </form>
    </section>
  );
}

export default ContactComponent;
