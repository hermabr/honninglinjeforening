import Image from "next/image";
import Link from "next/link";
import React from "react";

function Hero() {
  return (
    <section
      id="hero"
      // className="bg-[#f4f2ed] flex flex-col justify-center px-4 pt-24 text-center md:mt-0 md:h-screen md:text-left md:flex-row md:justify-between md:items-center lg:px-48 md:px-12 bg-secondary"
      className="flex flex-col justify-center px-4 pt-24 text-center md:mt-0 md:h-screen md:text-left md:flex-row md:justify-between md:items-center lg:px-48 md:px-12 bg-secondary"
    >
      <div className="md:flex-1 md:mr-10">
        <h1 className="text-4xl font-bold font-pt-serif mb-7 text-gold">
          Honours-programmet
          <br />
          <span
            className="bg-underline1 bg-left-bottom bg-no-repeat pb-2 bg-100% "
            style={{ backgroundImage: "url('/images/svgs/Underline1.svg')" }}
          >
            sin linjeforening
          </span>
        </h1>
        <p className="font-normal font-pt-serif mb-7 text-neutral-100">
          Honours-programmet er et tverrfaglig studium ved Universitetet i Oslo
          med studenter innenfor humaniora, samfunnsvitenskap og realfag.
        </p>
        <div className="font-montserrat">
          {/* <Link href={"https://www.youtube.com/watch?v=dQw4w9WgXcQ"}> */}
          <Link href="/kontakt">
            <a className="px-6 py-4 mb-2 mr-2 text-black border-2 border-solid rounded-lg bg-gold border-gold">
              Kontakt oss
            </a>
          </Link>
        </div>
      </div>
      <div className="flex justify-center">
        <div className="flex flex-col justify-center max-w-xl">
          <div className="self-start">
            <Image
              src="/images/svgs/Highlight1.svg"
              className="self-start"
              alt=""
              width="55"
              height="58"
            />
          </div>
          <div className="mx-12">
            <Image
              src="/images/gruppebilde.jpeg"
              className="rounded-md"
              alt="Gruppebilde av honours-studenter"
              width="465"
              height="372"
            />
          </div>
          <div className="self-end">
            <Image
              src="/images/svgs/Highlight2.svg"
              alt=""
              width="55"
              height="58"
            />
          </div>
        </div>
      </div>
    </section>
  );
}

export default Hero;
