import Image from "next/image";
import Link from "next/link";
import React from "react";

function Navbar() {
  return (
    <>
      <nav className="fixed z-10 flex content-center justify-between w-full px-4 py-6 lg:px-48 md:px-12 bg-secondary">
        <div className="flex items-center">
          <Link href={"/"}>
            <a>
              <Image
                src={"/images/logo.png"}
                alt="Logo"
                className="h-4"
                width="87"
                height="35"
              />
            </a>
          </Link>
        </div>
        <div className="font-montserrat">
          <Link href="/om-oss">
            <a className="mr-6 text-neutral-100">Om oss</a>
          </Link>
          <Link href="/kontakt">
            <a className="px-4 py-2 text-black bg-neutral-100 rounded-3xl">
              Kontakt oss
            </a>
          </Link>
        </div>
      </nav>
    </>
  );
}

export default Navbar;
