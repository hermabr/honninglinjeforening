/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  async redirects() {
    return [
      {
        source: '/porno',
        destination: '/oskar-ost',
        permanent: true,
      },
      {
        source: '/pornokjeller',
        destination: '/oskar-ost',
        permanent: true,
      },
      {
        source: '/porn',
        destination: '/oskar-ost',
        permanent: true,
      },
    ]
  },
}

module.exports = nextConfig
